# Is abusing the collumn type costing you performance?

I got curious whatever using a ```VARCHAR``` as a type to store integer values has any performance penalty. I was migrating customers' database and fallen into trap of doing ```object.value == 1``` when the correct expression would be ```object.value == "1"``` as the ORM correctly returned ```str``` and not ```int```.

I generated million of random inserts into one-field tables, without indexes at first, then I checked if the average comes to five,

*TL;DR*: Mixing types while you lookup will make your queries crawl

```
mysql emil@127.0.0.1:test-scan> SELECT AVG(test) FROM sole_int
+-------------+
|   AVG(test) |
|-------------|
|      5.0051 |
+-------------+
```
With ```query_cache``` disabled for all my tests I did the following:

## Three numbers against unindexed field of strings

```
mysql emil@127.0.0.1:test-scan> select count(1) from sole_string where test = 1 
+------------+
|   count(1) |
|------------|
|      90801 |
+------------+
1 row in set
Time: 0.420s
mysql emil@127.0.0.1:test-scan> select count(1) from sole_string where test = 5 
+------------+
|   count(1) |
|------------|
|      90572 |
+------------+
1 row in set
Time: 0.432s
mysql emil@127.0.0.1:test-scan> select count(1) from sole_string where test = 10 
+------------+
|   count(1) |
|------------|
|      91311 |
+------------+
1 row in set
Time: 0.448s
```

## Three numbers against unindexed field of ints

```
mysql emil@127.0.0.1:test-scan> select count(1) from sole_int where test = 1 
+------------+
|   count(1) |
|------------|
|      90801 |
+------------+
1 row in set
Time: 0.402s
mysql emil@127.0.0.1:test-scan> select count(1) from sole_int where test = 5
+------------+
|   count(1) |
|------------|
|      90572 |
+------------+
1 row in set
Time: 0.415s
mysql emil@127.0.0.1:test-scan> select count(1) from sole_int where test = 10
+------------+
|   count(1) |
|------------|
|      91311 |
+------------+
1 row in set
Time: 0.430s
```

No much difference as one could guess, there's a tiny speed up on ints but nothing that drastic.

## Strings with hash type indexes

```
mysql emil@127.0.0.1:test-scan> select count(1) from sole_string where test = 1
+------------+
|   count(1) |
|------------|
|      90801 |
+------------+
1 row in set
Time: 0.479s
mysql emil@127.0.0.1:test-scan> select count(1) from sole_string where test = 5
+------------+
|   count(1) |
|------------|
|      90572 |
+------------+
1 row in set
Time: 0.472s
mysql emil@127.0.0.1:test-scan> select count(1) from sole_string where test = 10
+------------+
|   count(1) |
|------------|
|      91311 |
+------------+
1 row in set
Time: 0.444s
```

## Ints with hash type index

```
mysql emil@127.0.0.1:test-scan> select count(1) from sole_int where test = 1
+------------+
|   count(1) |
|------------|
|      90801 |
+------------+
1 row in set
Time: 0.089s
mysql emil@127.0.0.1:test-scan> select count(1) from sole_int where test = 5
+------------+
|   count(1) |
|------------|
|      90572 |
+------------+
1 row in set
Time: 0.082s
mysql emil@127.0.0.1:test-scan> select count(1) from sole_int where test = 10
+------------+
|   count(1) |
|------------|
|      91311 |
+------------+
1 row in set
Time: 0.084s
```

The difference is easy to spot. Using BTREE makes no difference. Of course using the ```"5"``` or ```"10"``` values for a lookup in ```sole_string``` "fixes" the lookup time but… don't mix types, mix alcohol.
