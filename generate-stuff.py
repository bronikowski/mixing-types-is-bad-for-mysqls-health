import random

tables = (
        'sole_int',
        'sole_string',
        'ai_and_int',
        'ai_and_string'
)

test_values = []

for i in range(0, 1000 * 1000):
    test_values.append(random.randint(0, 10))

with open('test.sql', 'w') as f:

    for table in tables:

        for i in test_values:
            query = "INSERT INTO {}(`test`) VALUES ({});\n".format(
                    table,
                    i
            )
            f.write(query)
